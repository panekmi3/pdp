#!/bin/bash

# Nastavení řídících parametrů plánovače Slurm
#SBATCH --job-name=tsk
#SBATCH --output="./runs/tsk/%x-%J.out"
#SBATCH --error="./runs/tsk/%x-%J.err"
#SBATCH --exclusive

# Aktivace HPE CPE
source /etc/profile.d/zz-cray-pe.sh

export MV2_HOMOGENEOUS_CLUSTER=1
export MV2_SUPPRESS_JOB_STARTUP_PERFORMANCE_WARNING=1
#export CRAY_OMP_CHECK_AFFINITY=TRUE

# Nastavení proměných prostředí pro naplánovanou úlohu
#module load cray-mvapich2_pmix_nogpu/2.3.7
module load cray-mvapich2_pmix_nogpu

export MV2_ENABLE_AFFINITY=0

# Za příkazem srun napsat cestu k programu i s jeho argumenty, který se má spustit na naplánovaných výpočetních uzlech
srun ./tsk "$2" "$1"

exit 0