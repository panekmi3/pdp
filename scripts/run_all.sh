#!/bin/bash

datasets=(
    "data/in_0015.txt"
    "data/in_x001.txt"
    "data/in_x005.txt"
    )


threads=(
    1
    2
    4
    8
    16
    24
    32
    48
)

nodes=(
    3
    4
    6
)

#run all serial tests
for dataset in ${datasets[@]}
do
    sbatch -p "arm_serial" "scripts/seq.sh" "$dataset" 0

    for t in ${threads[@]}
    do
        sbatch -p "arm_long" "scripts/tsk.sh" "$dataset" "$t"
        sbatch -p "arm_long" "scripts/dat.sh" "$dataset" "$t"
    done
    for n in ${nodes[@]}
    do
        sbatch -p "arm_long" -N "$n" "scripts/mpi.sh" "$dataset" 6
        sbatch -p "arm_long" -N "$n" "scripts/mpi.sh" "$dataset" 12
        sbatch -p "arm_long" -N "$n" "scripts/mpi.sh" "$dataset" 24
        sbatch -p "arm_long" -N "$n" "scripts/mpi.sh" "$dataset" 48
    done 
done


