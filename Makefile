include config.mk

seq: src/seq.cpp
	$(CC) $(CXXFLAGS) src/seq.cpp -o seq -O3 

tsk: src/tsk.cpp
	$(CC) $(CXXFLAGS) src/tsk.cpp -o tsk -O3 -fopenmp

dat: src/dat.cpp
	$(CC) $(CXXFLAGS) src/dat.cpp -o dat -O3 -fopenmp

mpi: src/mpi.cpp
	mpiCC $(CXXFLAGS) src/mpi.cpp -o mpi -O3 -fopenmp

run-seq: seq
	./seq 0 data/in_0001.txt -v

run-tsk: tsk;
	./tsk 0 data/in_0001.txt -v

run-dat: dat
	./dat 0 data/in_0001.txt -v

clean: 
	rm tsk seq dat mpi build/*.o
