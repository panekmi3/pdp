#include "common.hpp"
#include <omp.h>

#define MAX_DEPTH 1

size_t upper_bound;
size_t g_upper_bound;
PBoard g_bestState = nullptr;
size_t g_bestDepth = SIZE_MAX;

size_t g_count = 0;

std::vector<CBoard> g_boards;

void solve(CBoard & board, size_t depth = 0)
{   
    if(depth < g_bestDepth && board.isSolution())
    {
        #pragma omp critical
        {
            if (depth < g_bestDepth)
            {
                g_upper_bound = depth;
                g_bestState = std::make_shared<CBoard>(board);
                std::cout << "found solution " << depth << std::endl;
            }
        } 
    }

    if( board.lowerBound() + depth >= g_upper_bound ) return;

    std::vector<CBoard> boards;
    boards.reserve(16);

    if(board.whiteToMove && ! board.whiteSolved())
    {
        for (auto &&knight : board.WKnights)
        {
            if(board.isGoal(knight, true)) continue;
            for (auto &&move : precalculatedMoves[knight])
            {
                if( ! board.squareFree(move)) continue;

                auto newBoard = board.makeMove(knight, move);
                
                auto lb = lower_bound(boards.begin(), boards.end(), *newBoard);
                boards.insert(lb, std::move(*newBoard));
            }
        }
    }
    else if( ! board.blackSolved() )
    {
        for (auto &&knight : board.BKnights)
        {
            if(board.isGoal(knight, false)) continue;
            for (auto &&move : precalculatedMoves[knight])
            {
                if( ! board.squareFree(move)) continue;

                auto newBoard = board.makeMove(knight, move);

                auto lb = lower_bound(boards.begin(), boards.end(), *newBoard);
                boards.insert(lb, std::move(*newBoard));
            }  
        }
    }

    for (auto &&b : boards)
    {
        solve(b, depth + 1);
    }
}

void pregenerateBoards(CBoard board, size_t depth = 0)
{
    if(depth > MAX_DEPTH || g_boards.size() > 8000) return;

    if(board.whiteToMove && ! board.whiteSolved())
    {
        
        for (auto &&knight : board.WKnights)
        {
            if(board.isGoal(knight, true)) continue;
            for (auto &&move : precalculatedMoves[knight])
            {
                if( ! board.squareFree(move)) continue;

                auto newBoard = board.makeMove(knight, move);
                
                g_boards.push_back(*newBoard);
            }
        }
    }

    else if( ! board.blackSolved() )
    {
        for (auto &&knight : board.BKnights)
        {
            if(board.isGoal(knight, false)) continue;
            for (auto &&move : precalculatedMoves[knight])
            {
                if( ! board.squareFree(move)) continue;

                auto newBoard = board.makeMove(knight, move);

                g_boards.push_back(*newBoard);
            }
        }
    }
    
    size_t size_now = g_boards.size();

    for(size_t i = g_count ; i < size_now; i++)
    {
        pregenerateBoards(g_boards[i], depth + 1);
    }
    g_count = size_now;
}

void solve_wrapper(CBoard & board, size_t threads)
{
    g_boards.reserve(8196);
    pregenerateBoards(board, 0);
    //std::cout << "count: " << g_boards.size() << std::endl;
    std::sort(g_boards.begin(), g_boards.end());

    #pragma omp parallel for num_threads(threads) schedule(dynamic)
    for (size_t i = 0; i < g_boards.size(); i++)
    {
        solve(g_boards[i], g_boards[i].history.size());
    }
    
}

int main(int argc, char const *argv[])
{   
    using namespace std;
    if(argc < 3 || argc >4)
    {
        cout << "Ivalid arguments!" << endl;
        return 1;
    }
    string filename = argv[2];
    size_t threads = atoi(argv[1]);
    bool verbose = false;
    if(argc > 3) verbose = strcmp(argv[3], "-v") == 0;

    SInstance instance;
    loadInstance(&instance, filename);
    

    precalculateMoves(instance);
    calculateDistances(instance);

    g_upper_bound = calculateUpperBound(instance);
    precalculteGoalDistances(instance);

    CBoard board(instance);
    //std::cout << board << std::endl;
    //std::cout << g_upper_bound << std::endl;
    //std::cout << board.lowerBound() << std::endl;
    auto start = std::chrono::system_clock::now();

    solve_wrapper(board, threads);

    auto end = std::chrono::system_clock::now();
    auto duration = chrono::duration_cast<chrono::milliseconds>(end - start).count();
    
    if (g_bestState && verbose) printHistory(instance, * g_bestState);
    std::cerr << filename << ", threads: " << threads <<std::endl;
    std::cout << (( verbose ) ? "Found: ":"") << g_upper_bound << endl;
    
    if(verbose)
    {
        size_t millis  = (duration%1000);
        size_t seconds = (duration%60000)/1000;
        size_t minutes =  duration/60000;
        std::cerr << "Time: ";
        std::cerr << minutes << "m"; 
        std::cerr << seconds << ".";
        std::cerr << std::setw(3) << std::setfill('0') << millis  << "s"; 
        std::cerr << endl; 
    }
    else
        std::cerr << duration << endl;

    return 0;
}