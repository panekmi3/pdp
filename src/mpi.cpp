#include <omp.h>
#include <mpi.h>
#include <string>
#include "common.hpp"


using namespace std;

#define BUFFER_SIZE 2048

#define INSTANCE 0
#define NEW_WORK 1
#define BEST_DEPTH_UPDATE 2
#define END 5
#define SOLUTION_FOUND 10
#define WORK_COMPLETED 11
#define AFTER_END 12
//#define NEW_WORK 1

struct message
{
    size_t best_depth;
    std::byte board[BUFFER_SIZE];
};

size_t g_upper_bound;
PBoard g_bestState = nullptr;

size_t g_count = 0;
size_t my_rank, comm_size;
std::vector<CBoard> g_boards;

void solve(CBoard & board, size_t depth = 0)
{   
    #pragma omp critical
    {  
        size_t recieved = 100000;
        MPI_Status status;
        status.MPI_TAG = 0;
        int flag = 0;
        MPI_Iprobe(0, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);
        if(flag && status.MPI_TAG == BEST_DEPTH_UPDATE)
        {
            //cout << "Worker: " << my_rank << " recieved updated best depth" << endl;
            
            MPI_Recv(&recieved, sizeof(recieved), MPI_BYTE, 0, BEST_DEPTH_UPDATE, MPI_COMM_WORLD, &status);
            //cout << "Worker: " << my_rank << " new best depth:" << recieved << endl;
            if(recieved < g_upper_bound) g_upper_bound = recieved;
        }
    }
    
    if( board.lowerBound() + depth >= g_upper_bound ) return;

    if(board.isSolution() && depth < g_upper_bound)
    {
        #pragma omp critical
        {
            if (depth < g_upper_bound)
            {
                g_upper_bound = depth;
                g_bestState = std::make_shared<CBoard>(board);

                message m;
                m.best_depth = g_upper_bound;
                g_bestState->serialize((size_t*)m.board);
                MPI_Send(&m, sizeof(message), MPI_BYTE, 0, SOLUTION_FOUND, MPI_COMM_WORLD);
            }
        } 
    }

    std::vector<CBoard> boards;
    boards.reserve(16);

    if(board.whiteToMove && ! board.whiteSolved())
    {
        for (auto &&knight : board.WKnights)
        {
            if(board.isGoal(knight, true)) continue;
            for (auto &&move : precalculatedMoves[knight])
            {
                if( ! board.squareFree(move)) continue;

                auto newBoard = board.makeMove(knight, move);
                
                auto lb = lower_bound(boards.begin(), boards.end(), *newBoard);
                boards.insert(lb, std::move(*newBoard));
            }
        }
    }
    else if( ! board.blackSolved() )
    {
        for (auto &&knight : board.BKnights)
        {
            if(board.isGoal(knight, false)) continue;
            for (auto &&move : precalculatedMoves[knight])
            {
                if( ! board.squareFree(move)) continue;

                auto newBoard = board.makeMove(knight, move);

                auto lb = lower_bound(boards.begin(), boards.end(), *newBoard);
                boards.insert(lb, std::move(*newBoard));
            }  
        }
    }

    for (auto &&b : boards)
    {
        if(depth + 1 >= g_upper_bound) return;
        solve(b, depth + 1);
    }
}

void pregenerateBoards(CBoard board, size_t depth, size_t max_num, size_t max_depth = 3)
{
    if(depth > max_depth || g_boards.size() > max_num) return;

    if(board.whiteToMove && ! board.whiteSolved())
    {
        
        for (auto &&knight : board.WKnights)
        {
            if(board.isGoal(knight, true)) continue;
            for (auto &&move : precalculatedMoves[knight])
            {
                if( ! board.squareFree(move)) continue;

                auto newBoard = board.makeMove(knight, move);
                
                g_boards.push_back(*newBoard);
            }
        }
    }

    else if( ! board.blackSolved() )
    {
        for (auto &&knight : board.BKnights)
        {
            if(board.isGoal(knight, false)) continue;
            for (auto &&move : precalculatedMoves[knight])
            {
                if( ! board.squareFree(move)) continue;

                auto newBoard = board.makeMove(knight, move);

                g_boards.push_back(*newBoard);
            }
        }
    }
    
    size_t size_now = g_boards.size();

    for(size_t i = g_count ; i < size_now; i++)
    {
        pregenerateBoards(g_boards[i], depth + 1, max_num, max_depth);
    }
    g_count = size_now;
}

void solve_wrapper(CBoard & board, size_t threads)
{
    g_boards.reserve(8196);
    pregenerateBoards(board, 0, 2000, 3);
    std::sort(g_boards.begin(), g_boards.end());
    //cout << "Worker: " << my_rank << " called solveW with upper bound: " << g_upper_bound << endl;
    //std::cout << threads << std::endl; 
    #pragma omp parallel for num_threads(threads) schedule(dynamic)
    for (size_t i = 0; i < g_boards.size(); i++)
    {
        solve(g_boards[i], g_boards[i].history.size());
    }
}

int main(int argc, char *argv[])
{
    MPI_Init( &argc, &argv );

    MPI_Comm_rank(MPI_COMM_WORLD, (int * )&my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, (int * )&comm_size);
    if(my_rank == 0)
    {
        auto start = std::chrono::system_clock::now();
        //std::cout << "main started" << std::endl;
        MPI_Status status;
        string filename = argv[2];
        SInstance instance;
        size_t threads = atoi(argv[1]);
        instance.threads = threads;
        loadInstance(&instance, filename);
        CBoard board(instance);
        precalculateMoves(instance);
        calculateDistances(instance);
        g_upper_bound = calculateUpperBound(instance);
        precalculteGoalDistances(instance);

        //send instance to all workers
        for (size_t worker = 1; worker < comm_size; worker++)
        {
            MPI_Send(&instance, sizeof(SInstance), MPI_BYTE, worker, 0, MPI_COMM_WORLD);
        }
        
        pregenerateBoards(board, 0, SIZE_MAX, 1);
        size_t i = 0;
        for ( ; i < g_boards.size() ; i++)
        {
            if(g_boards[i].history.size() == 2)                
                break;
        }
        vector<CBoard> tmp(g_boards.begin() + i, g_boards.end());
        g_boards = std::move(tmp);
        std::sort(g_boards.begin(), g_boards.end());

        //send first batch of workloads
        size_t b = 0;
        for (size_t worker = 1; worker < comm_size && b < g_boards.size(); worker++)
        {
            message m;
            m.best_depth = SIZE_MAX;
            g_boards[b++].serialize((size_t*)m.board);
            MPI_Send(&m, sizeof(m), MPI_BYTE, worker, 1, MPI_COMM_WORLD);
        }

        //cout << "first batch sent" << endl;

        message result;
        while(true)
        {
            MPI_Recv(&result, sizeof(message), MPI_BYTE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

            if( result.best_depth < g_upper_bound ) 
            {
                g_upper_bound = result.best_depth;
                g_bestState = CBoard::deserialize((size_t*)result.board);
                //cout << "-----------------------Recieved new best: " << g_upper_bound << endl;

                for (size_t worker = 1; worker < comm_size; worker++)
                {
                    MPI_Send(&g_upper_bound, sizeof(g_upper_bound), MPI_BYTE, worker, BEST_DEPTH_UPDATE, MPI_COMM_WORLD);
                }
            }
            if (status.MPI_TAG == WORK_COMPLETED)
            {
                //cout << "worker "<< status.MPI_SOURCE << " completed work" << endl;
                message m;
                m.best_depth = g_upper_bound;
                b++;
                if(b == g_boards.size()) break;
                g_boards[b].serialize((size_t*)m.board);
                MPI_Send(&m, sizeof(message), MPI_BYTE, status.MPI_SOURCE, NEW_WORK, MPI_COMM_WORLD);
            }
        }

        //cout << "board buffer empty" << endl;
        //send ending packet to allworkers
        for (size_t worker = 1; worker < comm_size; worker++)
        {
            MPI_Send(NULL, 0, MPI_BYTE, worker, NEW_WORK, MPI_COMM_WORLD);
        }

        for (size_t worker = 1; worker < comm_size; worker++)
        {
            MPI_Recv(&result, sizeof(message), MPI_BYTE, MPI_ANY_SOURCE, AFTER_END, MPI_COMM_WORLD, &status);
            if( result.best_depth < g_upper_bound ) 
            {
                g_upper_bound = result.best_depth;
                g_bestState = CBoard::deserialize((size_t*)result.board);
            }
        }
        
        //if(g_bestState) printHistory(instance, *g_bestState);
        cout << g_upper_bound << endl;
        auto end = std::chrono::system_clock::now();

        auto duration = chrono::duration_cast<chrono::milliseconds>(end - start).count();
        std::cerr << filename << "threads: " << threads << std::endl;
        std::cerr << duration << endl;

        //cout << "master ends" << endl;
    }
    else
    {
        MPI_Status status;
        SInstance instance;

        MPI_Recv(&instance, sizeof(SInstance), MPI_BYTE, MPI_ANY_SOURCE, INSTANCE, MPI_COMM_WORLD, &status);
        
        size_t threads = instance.threads;

        precalculateMoves(instance);
        calculateDistances(instance);
        g_upper_bound = calculateUpperBound(instance);
        precalculteGoalDistances(instance);
        
        //cout << "Worker: " << my_rank << " upper: "<< g_upper_bound << endl;

        message m;
        
        while( true )
        {
            //cout << "Worker: " << my_rank << " waiting for work" << endl;
            MPI_Recv(&m, sizeof(message), MPI_BYTE, MPI_ANY_SOURCE, NEW_WORK, MPI_COMM_WORLD, &status);
            //cout << "Worker: " << my_rank << " recieved work" << endl;

            int count = 0;
            MPI_Get_count(&status, MPI_BYTE, &count);
            if( ! count ) break;

            auto board = CBoard::deserialize((size_t*)m.board);
            if(m.best_depth < g_upper_bound) g_upper_bound = m.best_depth;

            solve_wrapper(*board, threads);

            m.best_depth = g_upper_bound;
            if(g_bestState)
                g_bestState->serialize((size_t*)m.board);
            //cout << "Worker: " << my_rank << " finished work" << endl;
            MPI_Send(&m, sizeof(message), MPI_BYTE, 0, WORK_COMPLETED, MPI_COMM_WORLD);
            //cout << "Worker: " << my_rank << " sent found solution" << endl;
        }
        //cout << "Worker: " << my_rank << " recieved end packet" << endl;
        m.best_depth = g_upper_bound;
        if(g_bestState)
            g_bestState->serialize((size_t*)m.board);

        MPI_Send(&m, sizeof(message), MPI_BYTE, 0, AFTER_END, MPI_COMM_WORLD);
        //cout << "Worker: " << my_rank << " sent final solution" << endl;

    }
    MPI_Finalize();
    return 0;
}
