#include "common.hpp"

#include <omp.h>

size_t upper_bound;
size_t g_upper_bound;
PBoard g_bestState = nullptr;
size_t g_bestDepth = SIZE_MAX;

void solve(CBoard & board, size_t depth)
{   
    if(board.isSolution() && depth < g_bestDepth)
    {
        #pragma omp critical
        {
            if (depth < g_bestDepth)
            {
                g_upper_bound = depth;
                g_bestState = std::make_shared<CBoard>(board);
            }
        } 
    }

    if( board.lowerBound() + depth >= g_upper_bound ) return;

    std::vector<CBoard> boards;
    boards.reserve(16);

    if(board.whiteToMove && ! board.whiteSolved())
    {
        for (auto &&knight : board.WKnights)
        {
            if(board.isGoal(knight, true)) continue;
            for (auto &&move : precalculatedMoves[knight])
            {
                if( ! board.squareFree(move)) continue;

                auto newBoard = board.makeMove(knight, move);
                
                auto lb = lower_bound(boards.begin(), boards.end(), *newBoard);
                boards.insert(lb, std::move(*newBoard));
            }
        }
    }
    else if( ! board.blackSolved() )
    {
        for (auto &&knight : board.BKnights)
        {
            if(board.isGoal(knight, false)) continue;
            for (auto &&move : precalculatedMoves[knight])
            {
                if( ! board.squareFree(move)) continue;

                auto newBoard = board.makeMove(knight, move);

                auto lb = lower_bound(boards.begin(), boards.end(), *newBoard);
                boards.insert(lb, std::move(*newBoard));
            }  
        }
    }
    auto maxprio = omp_get_max_task_priority();
    for (size_t i = 0; i < boards.size(); i++)   
    {
        # pragma omp task if (depth < 5) priority(maxprio - i)
        solve(boards[i], depth + 1);
    }
}

void solve_wrapper(CBoard & board, size_t threads)
{
    if (threads == 0) threads = omp_get_num_procs();
    #pragma omp parallel num_threads(threads)
    {
        #pragma omp single
        {
            solve(board, 0);        
        }
    }
}

int main(int argc, char const *argv[])
{   
    using namespace std;
    if(argc < 3 || argc >4)
    {
        cout << "Ivalid arguments!" << endl;
        return 1;
    }
    string filename = argv[2];
    size_t threads = atoi(argv[1]);
    bool verbose = false;
    if(argc > 3) verbose = strcmp(argv[3], "-v") == 0;

    SInstance instance;
    loadInstance(&instance, filename);
    

    precalculateMoves(instance);
    calculateDistances(instance);

    g_upper_bound = calculateUpperBound(instance);
    precalculteGoalDistances(instance);

    CBoard board(instance);
    
    auto start = std::chrono::system_clock::now();

    solve_wrapper(board, threads);

    auto end = std::chrono::system_clock::now();
    auto duration = chrono::duration_cast<chrono::milliseconds>(end - start).count();
    
    if (g_bestState && verbose) printHistory(instance, * g_bestState);
    std::cerr << filename << ", threads: " << threads <<std::endl;
    std::cout << (( verbose ) ? "Found: ":"") << g_upper_bound << endl;
    
    if(verbose)
    {
        size_t millis  = (duration%1000);
        size_t seconds = (duration%60000)/1000;
        size_t minutes =  duration/60000;
        std::cerr << "Time: ";
        std::cerr << minutes << "m"; 
        std::cerr << seconds << ".";
        std::cerr << std::setw(3) << std::setfill('0') << millis  << "s"; 
        std::cerr << endl; 
    }
    else
        std::cerr << duration << endl;

    return 0;
}