#ifndef COMMON_H
#define COMMON_H 1

#pragma once

#include <iostream>
#include <vector>
#include <memory>
#include <algorithm>
#include <string>
#include <fstream>
#include <set>
#include <map>
#include <assert.h>
#include <queue>
#include <array>
#include <string.h>
#include <iomanip>
#include <chrono>

//#include"CBoard.hpp"
class CBoard;
struct SInstance;

#define MAX_SIZE 20

typedef std::pair<size_t, size_t> point;
typedef std::pair<point, point> move_t;
std::ostream & operator<<(std::ostream & stream, point p);

std::array<std::array<size_t, MAX_SIZE>, MAX_SIZE> whiteLookup, blackLookup;
std::map<point, std::vector<point>> precalculatedMoves;
std::map<std::pair<point, point>, size_t> distances;
std::array<std::array<size_t, MAX_SIZE>, MAX_SIZE> WGoal_array, BGoal_array;
std::set<point> WGoal, BGoal;

void print_bytes(std::ostream& out, const char *title, std::byte *data, size_t dataLen, bool format = true) {
    out << title << std::endl;
    out << std::setfill('0');
    for(size_t i = 0; i < dataLen; ++i) {
        out << std::hex << std::setw(2) << (int)data[i];
        if (format) {
            out << (((i + 1) % 16 == 0) ? "\n" : " ");
        }
    }
    out << std::endl;
}

struct SInstance
{
    size_t m, n, k ,threads = 0;
    std::pair<point, point> W, B;
    friend std::ostream& operator<<(std::ostream& stream, SInstance instance);
};

void precalculateMoves(const SInstance& instance)
{
    static int X[8] = { 2, 1, -1, -2, -2, -1, 1, 2 };
    static int Y[8] = { 1, 2, 2, 1, -1, -2, -2, -1 };
    for (size_t x = 0; x < instance.m; x++)
    {
        for (size_t y = 0; y < instance.n; y++)
        {
            for (size_t i = 0; i < 8; i++)
            {
                size_t a = x + X[i];
                size_t b = y + Y[i];

                if (a >= 0 && b >= 0 && a < instance.m && b < instance.n)
                    precalculatedMoves[{x,y}].push_back({a,b});
            }
        }
    }
}

void calculateDistances(const SInstance& instance)
{
    for (size_t x = 0; x < instance.m; x++)
    {
        for (size_t y = 0; y < instance.n; y++)
        {
            point start = {x,y};
            distances[{start, start}] = 0;
            std::queue<point> Q;
            std::set<point> visited;
            Q.push(start);
            visited.insert(start);
            while ( ! Q.empty())
            {
                auto v = Q.front();
                Q.pop();
                for (auto &&w : precalculatedMoves[v])
                {
                    if(visited.count(w)) continue;
                    visited.insert(w);
                    distances[{start, w}] = distances[{start, v}] + 1;
                    Q.push(w);
                }
            }
        }
    }
}

void loadInstance(SInstance * instance, std::string filename)
{
    std::string line;

    auto inFile = std::ifstream(filename);

    inFile >> instance->m >> instance->n >> instance->k >> instance->k;
    size_t a, b, c, d;
    inFile >> a >> b >> c >> d;
    instance->W.first = {a, b};
    instance->W.second = {c, d};
    inFile >> a >> b >> c >> d;
    instance->B.first = {a, b};
    instance->B.second = {c, d};
}

void precalculteGoalDistances(const SInstance& instance )
{

    for (size_t x = 0; x < instance.m; x++)
    {
        for (size_t y = 0; y < instance.n; y++)
        {
            size_t min = 10000;
            for (auto &&goal : WGoal)
            {                    
                size_t distance = distances[{{x,y},goal}];
                if ( min > distance )
                    min = distance;
            }
            whiteLookup[x][y] = min;
            min = 10000;
            for (auto &&goal : BGoal)
            {
                size_t distance = distances[{{x,y},goal}];
                if ( min > distance )
                    min = distance;
            }
            blackLookup[x][y] = min;
        }            
    }
}

class CBoard    
{
public:
    CBoard() = default;

    CBoard(const SInstance& instance)
    {
        assert(instance.k > 0);
        this->m = instance.m;
        this->n = instance.n;
        for (size_t x = instance.B.first.first; x <= instance.B.second.first; x++)
        {
            for (size_t y = instance.B.first.second; y <= instance.B.second.second; y++)
            {
                this->BKnights.insert({x,y});
                WGoal_array[x][y] = 1;
                WGoal.insert({x,y});
            }
        }

        for (size_t x = instance.W.first.first; x <= instance.W.second.first; x++)
        {
            for (size_t y = instance.W.first.second; y <= instance.W.second.second; y++)
            {
                this->WKnights.insert({x,y});
                BGoal_array[x][y] = 1;
                BGoal.insert({x,y});
            }
        }
        this->whiteToMove = true;
    }

    bool isGoal(const point p, bool white) const
    {
        if(white) return WGoal_array[p.first][p.second];
        return BGoal_array[p.first][p.second];
    }

    bool blackSolved() const
    {
        for (auto &&k : BKnights)
            if ( ! isGoal(k, false)) return false;
        return true;
    }

    bool whiteSolved() const
    {
        for (auto &&k : WKnights)
            if ( ! isGoal(k, true)) return false;
        return true;
    }

    bool isSolution() const
    {
        return whiteSolved() && blackSolved();
    }

    std::vector<point> getMoves(const point piece) const
    {
        return precalculatedMoves[piece];
    }

    bool squareFree(const point p) const
    {
        return (BKnights.count(p) == 0) && (WKnights.count(p) == 0);
    }

    typedef std::shared_ptr<CBoard> PBoard;
    PBoard makeMove(const point& from, const point& to) const
    {        
        PBoard newBoard = std::make_shared<CBoard>( * this);
        bool white = WKnights.count(from);
        if(white)
        {
            newBoard->WKnights.erase(from);
            newBoard->WKnights.insert(to);
        }
        else
        {
            newBoard->BKnights.erase(from);
            newBoard->BKnights.insert(to);
        }

        newBoard->whiteToMove = ! whiteToMove;
        newBoard->history.push_back(std::make_pair(from, to));

        return newBoard;
    }

    size_t lowerBound() const
    {
        size_t sum = 0;
        for (auto &&w : WKnights)
        {
            sum+= whiteLookup[w.first][w.second];
        }
        for (auto &&b : BKnights)
        {
            sum+= blackLookup[b.first][b.second];
        }

        return sum;
    }

    bool operator < (const CBoard& other) const
    {
        return this->lowerBound() < other.lowerBound();
    }

    size_t serialize( size_t  * array )
    {
        size_t size = 0;
        size_t expected_size = 6 * sizeof(size_t) + sizeof(point) * ( this->WKnights.size() + this->BKnights.size()) + sizeof(move_t) * this->history.size(); 

        assert(expected_size <= 2048);

        array[size++] = this->m;        
        array[size++] = this->n;
        array[size++] = (size_t) this->whiteToMove;

        array[size++] = this->WKnights.size();
        for (auto &&knight : this->WKnights)
        {   
            array[size++] = knight.first;
            array[size++] = knight.second;
        }

        array[size++] = this->BKnights.size();
        

        for (auto &&knight : this->BKnights)
        {   
            array[size++] = knight.first;
            array[size++] = knight.second;
        }

        array[size++] = this->history.size();

        for (auto &&move : this->history)
        {
            array[size++] = move.first.first;
            array[size++] = move.first.second;
            array[size++] = move.second.first;
            array[size++] = move.second.second;
        }

        assert(expected_size == (size * sizeof(size_t)));
        
        return size;
    }

    static PBoard deserialize( size_t * array)
    {
        size_t count = 0;
        CBoard board;

        board.m = array[count++];
        board.n = array[count++];
        board.whiteToMove = (bool) array[count++];

        size_t numberOfWknights = array[count++];
        for (size_t i = 0; i < numberOfWknights; i++)
        {
            point knight;
            knight.first = array[count++];
            knight.second = array[count++];
            board.WKnights.insert(knight);
        }

        size_t numberOfBknights = array[count++];
        for (size_t i = 0; i < numberOfBknights; i++)
        {
            point knight;
            knight.first = array[count++];
            knight.second = array[count++];
            board.BKnights.insert(knight);
        }

        size_t historySize = array[count++];
        for (size_t i = 0; i < historySize; i++)
        {
            point from, to;
            from.first = array[count++];
            from.second = array[count++];

            to.first = array[count++];
            to.second = array[count++];

            board.history.push_back({from, to});
        }

        return std::make_shared<CBoard>(board);
    }

    size_t m, n; 
    std::set<point> BKnights, WKnights;
    std::vector<move_t> history;
    bool whiteToMove;

    friend std::ostream & operator<<(std::ostream & stream, CBoard board);
};
typedef std::shared_ptr<CBoard> PBoard;

std::ostream & operator<<(std::ostream & stream, CBoard board)
{
    for (size_t y = 0; y < board.n; y++)
    {
        for (size_t x = 0; x < board.m; x++)
        {
            if( board.BKnights.count({x, y}) )
                stream << "B";
            else if (board.WKnights.count({x, y}))
                stream << "W";
            else
                stream << (( (x + y) % 2 ) ? ".":".");
        }
        stream << std::endl;
    }
    return stream;
}

size_t calculateUpperBound(const SInstance& instance)
{

    CBoard board = CBoard(instance);
    assert(precalculatedMoves.size() > 0);
    assert(distances.size() > 0);
    assert(board.BKnights.size() == board.WKnights.size());
    assert(board.BKnights.size() > 0);

    size_t sum = 0;

    for (auto &&w : board.WKnights)
    {
        size_t maxDistance = 0;
        for (auto &&b : board.BKnights)
        {
            if (distances[{w,b}] > maxDistance)
                maxDistance = distances[{w,b}]; 
        }
        sum += maxDistance;
    }

    for (auto &&b : board.WKnights)
    {
        size_t maxDistance = 0;
        for (auto &&w : board.WKnights)
        {
            if (distances[{b,w}] > maxDistance)
                maxDistance = distances[{w,b}]; 
        }
        sum += maxDistance;
    }
    return sum;
}

void printHistory(SInstance instance, CBoard bestBoard)
{
    PBoard board = std::make_shared<CBoard>(instance);
    std::cout <<  * board << std::endl;
    for (auto &&move : bestBoard.history)
    {
        board = board->makeMove(move.first, move.second);
        std::cout << * board << std::endl;
    }
}

std::ostream &operator<<(std::ostream &stream, SInstance instance)
{
    stream << instance.m << " " << instance.n << " " << instance.k << " " << instance.k << std::endl;
    stream << instance.W.first.first << " " << instance.W.first.second << " " << instance.W.second.first << " " << instance.W.second.second << std::endl;
    stream << instance.B.first.first << " " << instance.B.first.second << " " << instance.B.second.first << " " << instance.B.second.second << std::endl;

    return stream;
}

std::ostream & operator<<(std::ostream & stream, point p)
{
    stream << "(" << p.first << "," << p.second << ")"; 
    return stream;
}

#endif