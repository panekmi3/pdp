#include "common.hpp"

size_t upper_bound;
size_t g_upper_bound;
PBoard g_bestState = nullptr;
size_t g_bestDepth = SIZE_MAX;

size_t g_count = 0;

void solve(CBoard & board, size_t depth)
{   
    g_count++;
    if(depth < g_bestDepth && board.isSolution())
    {
        g_upper_bound = depth;
        g_bestState = std::make_shared<CBoard>(board);
    }

    if( board.lowerBound() + depth >= g_upper_bound ) return;

    std::vector<CBoard> boards;
    boards.reserve(16);

    if(board.whiteToMove && ! board.whiteSolved())
    {
        for (auto &&knight : board.WKnights)
        {
            if(board.isGoal(knight, true)) continue;
            for (auto &&move : precalculatedMoves[knight])
            {
                if( ! board.squareFree(move)) continue;

                auto newBoard = board.makeMove(knight, move);
                
                auto lb = lower_bound(boards.begin(), boards.end(), *newBoard);
                boards.insert(lb, std::move(*newBoard));
            }
        }
    }
    else if( ! board.blackSolved() )
    {
        for (auto &&knight : board.BKnights)
        {
            if(board.isGoal(knight, false)) continue;
            for (auto &&move : precalculatedMoves[knight])
            {
                if( ! board.squareFree(move)) continue;

                auto newBoard = board.makeMove(knight, move);

                auto lb = lower_bound(boards.begin(), boards.end(), *newBoard);
                boards.insert(lb, std::move(*newBoard));
            }  
        }
    }

    for (auto &&board : boards)
    {
        solve(board, depth + 1);
    }
}

void solve_wrapper(CBoard & board, size_t threads)
{
    threads = 0;
    solve(board, 0);
    //std::cout << g_count << std::endl;
}

int main(int argc, char const *argv[])
{   
    using namespace std;
    if(argc < 3 || argc >4)
    {
        cout << "Ivalid arguments!" << endl;
        return 1;
    }
    string filename = argv[2];
    size_t threads = atoi(argv[1]);
    bool verbose = false;
    if(argc > 3) verbose = strcmp(argv[3], "-v") == 0;

    SInstance instance;
    loadInstance(&instance, filename);
    

    precalculateMoves(instance);
    calculateDistances(instance);

    g_upper_bound = calculateUpperBound(instance);
    precalculteGoalDistances(instance);

    CBoard board(instance);
    
    auto start = std::chrono::system_clock::now();

    solve_wrapper(board, threads);

    auto end = std::chrono::system_clock::now();
    auto duration = chrono::duration_cast<chrono::milliseconds>(end - start).count();
    
    if (g_bestState && verbose) printHistory(instance, * g_bestState);
    std::cerr << filename << "threads: NA" << std::endl;
    std::cout << (( verbose ) ? "Found: ":"") << g_upper_bound << endl;
    
    if(verbose)
    {
        size_t millis  = (duration%1000);
        size_t seconds = (duration%60000)/1000;
        size_t minutes =  duration/60000;
        std::cerr << "Time: ";
        std::cerr << minutes << "m"; 
        std::cerr << seconds << ".";
        std::cerr << std::setw(3) << std::setfill('0') << millis  << "s"; 
        std::cerr << endl; 
    }
    else
        std::cerr << duration << endl;

    return 0;
}