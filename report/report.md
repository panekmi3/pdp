## Semestrální projekt NI-PDP LS 2023/2024

# Paralelní algoritmus pro řešení problému výměny jezdců na šachovnici

## Miloš Pánek, panekmi3

# 1 Definice problému a popis sekvenčního algoritmu
## Definice problému
Na vstupu algoritmu je zadána šachovnice určitých rozměrů na níž jsou umístěny dvě skupiny šachových jezdců. Účelem algoritmu je najít nejkratší posloupnost, tahů aby si skupiny jezdců vyměnili pozice. Jezdci se pohybují způsobem typickým pro šachového jezdce, tedy ve tvaru "L" dvě na tři políčka. Jezdci mohou také ostatní figurky přeskakovat, podobně jako podle běžných šachových pravidel. Pokud jsou všechny figury některého z hráčů již na svých cílových pozicích táhne druhý hráč opakovaně.

```
W........                     B........
W........                     B........
W.......B        ===>         B.......W
........B                     ........W
........B                     ........W
```

## Popis sekvenčního algoritmu

Algoritmus pracuje na bázi prohledávání stavového prostoru do hloubky. Hloubka vyhledávání je omezena horní mezí, aby nedošlo k zacyklení a aby nebyly prohledávány stavy které nevedou ke správnému řešení.

### Princip určení horní meze

Horní mez je na začátku výpočtu určena jako součet vzdáleností pro každého jezdce k jemu nejvzdálenějšímu cílovému políčku. Tyto vzdálenosti jsou uvažovány v počtu tahů potřebných k dosažení políčka. Dále je horní mez upravována jako hloubka nejlepšího nalezeného řešení.

### Třída CBoard

Třída uchovává aktuální stav šachovnice. Pozice jezdců jsou uchovány ve dvou množinách realizovaných jako ```std::set```. Každý jezdec je reprezentován pomocí bodu. Bod je ```std::pair<size_t, size_t>```. Dále třída uchovává, který z hráčů je na tahu a také historii tahů. Historie tahů je reprezentována jako ```std::vector<move>```. Move je ```std::pair<point, point>```.

### Pomocné struktury

Jedná se o několik struktur určených pro zrychlení výpočtu. Tyto struktury jsou jednou na začátku programu předpočítány a šetří tak čas během samotného výpočtu. Pro účely paralelizace se jedná o veřejné proměnné. V paralelní fázi programu jsou tyto struktury pouze čtené, není tak potřeba řešit jejich synchronizaci.

```std::set<point> WGoal, BGoal``` - Uchovává množinu bodů, které jsou pro černého/bílého hráče cílové. Množina umožňuje rychlé vyhledávání a je použita při určení, zda je daný stav šachovnice řešením.

```std::map<point, std::vector<point>> precalculatedMoves;``` -  Slovník, který pro každý bod na šachovnici vrátí pole tahů, které je možné z tohoto pole provést. Tahy jsou vráceny v podobě cílových políček. Použito při generování dostupných tahů pro daného jezdce.

```std::map<std::pair<point, point>, size_t> distances;``` - Slovník který pro dvojici bodů vrátí jejich vzdálenost v tazích. Použito při určování horní/dolní meze stavu.

```std::array<std::array<size_t, MAX_SIZE>, MAX_SIZE> whiteLookup, blackLookup;```- Pole uchovávající vzdálenosti z každého políčka k nejbližšímu cílovému políčku. Použito pro výpočet dolní meze stavu. 

## Popis prohledávání stavů

Stavy jsou prohledávány do hloubky rekurzivním způsobem pomocí funkce ````solve(CBoard, depth)````. Funkce přijímá aktuální stav v podobě třídy CBoard a aktuální hloubku. Pokud je stav řešením a je také nejlepším nalezeným řešením, je tento stav uchován jako dosavadní řešení. Pokud ne, zkontroluje se, zda spodní hranice stavu plus dosavadní hloubka nepřesahuje horní mez. Pokud ano, stav ani jeho podstavy nejsou dále uvažovány. V opačném případě jsou ze stavu vygenerovány nové stavy a to provedením každého možného tahu pro všechny jezdce. Tyto nové stavy jsou zařazeny do vektoru, který je řazen pomocí dolní meze těchto stavů. Následně je funkce volána rekurzivně pro tyto stavy. Slibné stavy jsou díky řazení prohledávány nejdříve. 

Pro rychlé provedení algoritmu je velice výhodné, pokud je nějaké řešení nalezeno co nejdříve. Pokud je řešení nalezeno, dojde ke snížení horní meze a algoritmus tak nemusí prohledávat stavy do takové hloubky. Protože počet stavů roste s hloubkou exponenciálně dojde tak také k masivnímu zkrácení doby potřebné pro prohledání celého stavového prostoru.

```c++
void solve(CBoard & board, size_t depth)
{   
    g_count++;
    if(depth < g_bestDepth && board.isSolution())
    {
        g_upper_bound = depth;
        g_bestState = std::make_shared<CBoard>(board);
    }

    if( board.lowerBound() + depth >= g_upper_bound ) return;

    std::vector<CBoard> boards;
    boards.reserve(16);

    if(board.whiteToMove && ! board.whiteSolved())
    {
        for (auto &&knight : board.WKnights)
        {
            if(board.isGoal(knight, true)) continue;
            for (auto &&move : precalculatedMoves[knight])
            {
                if( ! board.squareFree(move)) continue;

                auto newBoard = board.makeMove(knight, move);
                
                auto lb = lower_bound(boards.begin(), boards.end(), *newBoard);
                boards.insert(lb, std::move(*newBoard));
            }
        }
    }
    else if( ! board.blackSolved() )
    {
        for (auto &&knight : board.BKnights)
        {
            if(board.isGoal(knight, false)) continue;
            for (auto &&move : precalculatedMoves[knight])
            {
                if( ! board.squareFree(move)) continue;

                auto newBoard = board.makeMove(knight, move);

                auto lb = lower_bound(boards.begin(), boards.end(), *newBoard);
                boards.insert(lb, std::move(*newBoard));
            }  
        }
    }

    for (auto &&board : boards)
    {
        solve(board, depth + 1);
    }
}
```

# Task paralelismus

Algoritmus vznikl upravením programu pro sekvenční řešení přidáním OMP direktivy ```omp parallel``` pro prvotní volání funkce ```solve``` a direktivy ```òmp task``` pro rekurzivní volání. Nové tasky jsou vytvářeny pouze do hloubky 5. Další úpravou bylo přidání kritické sekce pro zápis řešení do sdílené proměnné.

# Datový paralelismus

Algoritmu využívá sekvenční verzi funkce solve doplněnou o kritickou sekci, podobně jako u task paralelismu. Jednotlivé stavy tak řeší sekvenčně, ovšem řeší takto více stavů najednou. Stavy jsou v první fázi programu sekvenčně vygenerovány a uloženy do vektoru. Tento vektor je poté seřazen podle spodní hranice a funkce solve je volána nad každým z těchto stavů pomocí OMP direktivy ```parallel for``` spolu s direktivou ```schedule(dynamic)```.

# MPI paralelismus

MPI verze programu se skládá z dvou částí, master a worker. Master část v první fázi sekvenčně vygeneruje stavy, které později posílá worker procesům. Generování stavů je prováděno do hloubky jedna. Worker procesy tyto stavy následně řeší tyto stavy pomocí algoritmu datového paralelismu. Pokud naleznou nějaké řešení, odešlou ho master procesu. Ten si řešení zaznamená a rozešle jeho hloubku všem ostatním worker procesům, aby neztrácely čas, hledáním hlubších řešení.

<div style="page-break-after: always;"></div>

# Měření a výsledky

## Instance

Pro měření byly zvoleny instance uvedené níže. instance jsou znázorněny graficky a je uvedena také reprezentace přijímána programy.


```
in_0015.txt         .....
5 4 4 4             BB.WW
3 1 4 2             BB.WW
0 1 1 2             .....
```

```
in_x001.txt         ......
6 4 4 4             BB..WW
4 1 5 2             BB..WW
0 1 1 2             ......
```

```
in_x005.txt         ....BBB
7 5 4 4             .......
0 2 0 4             W......
4 0 6 0             W......
                    W......
```

Všechny běhy programů, kromě sekvenčních, byly uskutečněny na arm clusteru ve frontě arm_long s desetiminutovým časovým limitem.

<div style="page-break-after: always;"></div>

## Výsledky měření

Následující tabulka udává délky jednotlivých běhů v milisekundách.



| type    | **nodes** | **threads** | **in_0015** | **in_x001** | **in_x005** |
|---------|-----------|-------------|-------------|-------------|-------------|
| **seq** | **NA**    | **NA**      | 285890      | 570205      | 621960      |
| **tsk** | **NA**    | **1**       | dnf         | dnf         | dnf         |
| **tsk** | **NA**    | **2**       | dnf         | dnf         | dnf         |
| **tsk** | **NA**    | **4**       | dnf         | dnf         | dnf         |
| **tsk** | **NA**    | **8**       | 52166       | dnf         | dnf         |
| **tsk** | **NA**    | **16**      | 20987       | 58002       | dnf         |
| **tsk** | **NA**    | **24**      | 6872        | 15          | 230488      |
| **tsk** | **NA**    | **32**      | 851         | 18          | 177182      |
| **tsk** | **NA**    | **48**      | 596         | 21          | 118059      |
| **dat** | **NA**    | **1**       | 10195       | 362         | dnf         |
| **dat** | **NA**    | **2**       | 6409        | 406         | dnf         |
| **dat** | **NA**    | **4**       | 2736        | 7           | 386877      |
| **dat** | **NA**    | **8**       | 1503        | 8           | 215651      |
| **dat** | **NA**    | **16**      | 935         | 8           | 165593      |
| **dat** | **NA**    | **24**      | 846         | 9           | 149717      |
| **dat** | **NA**    | **32**      | 771         | 10          | 145425      |
| **dat** | **NA**    | **48**      | 729         | 40          | 144855      |
| **mpi** | **3**     | **6**       | 10841       | 786         | 330645      |
| **mpi** | **3**     | **12**      | 10301       | 837         | 294688      |
| **mpi** | **3**     | **24**      | 9529        | 801         | 285861      |
| **mpi** | **3**     | **48**      | 9646        | 1079        | 267220      |
| **mpi** | **4**     | **6**       | 4605        | 568         | 352647      |
| **mpi** | **4**     | **12**      | 4346        | 555         | 347121      |
| **mpi** | **4**     | **24**      | 4010        | 557         | 298639      |
| **mpi** | **4**     | **48**      | 3752        | 574         | 274952      |
| **mpi** | **6**     | **6**       | 3884        | 351         | 314537      |
| **mpi** | **6**     | **12**      | 3889        | 348         | 295727      |
| **mpi** | **6**     | **24**      | 3326        | 349         | 302371      |
| **mpi** | **6**     | **48**      | 4049        | 712         | 236656      |



<div style="page-break-after: always;"></div>

![Doba trvání dat pro in_0015.txt](imgs/dat_0015.png){width=100%}

![Doba trvání mpi pro in_0015.txt](imgs/mpi_0015.png){width=100%}

# Diskuze výsledků

## Taskový paralelismus

Taskový paralelismus se v měření ukázal jako velice špatný. Již při prvotních měřeních během vývoje se algoritmus ukázal jako velice nepředvídatelný s nekonzistentními dobami běhu. Příčinou je nejspíše plánovač vláken, který vlákna k běhu vybírá pseudonáhodně, lehce tak dojde k situaci, kdy k provedení vybere cestu grafem, která nevede k cíli. Běhy tsk které nedokončily výpočet byly opakovány, ovšem se stejným výsledkem.

## Datový paralelismus

Datový paralelizmus během měření vykazoval předpokládané zrychlení oproti sekvenčnímu řešení. Zajímavé je, že i běhy dat s jedním vláknem překonali sekvenční řešení. Domnívám sen, že tomu je protože prvotní fáze datového paralelismu generuje stavy k prozkoumání metodou do šířky až v paralelní fázi prohledává do hloubky. Tímto hybridním způsobem algoritmus dříve dosáhne nějakého řešení které umožní efektivnější ořezávání již nezajímavých stavů. Datový paralelizmus také v některých případech zrychluje více než lineárně s počtem vláken. Toto je způsobeno faktem, že vlákna nepracují nezávisle, tedy pokud například vlákno prohledávající pozdější část stavového prostoru narazí na řešení dojde ke zrychlení práce všech ostatních vláken. Sekvenční řešení ale ztrácí čas než kompletně prohledá předchozí část stavového prostoru.

## MPI paralelismus

MPI paralelismus vykazuje zrychlení oproti sekvenčnímu řešení již ovšem ne oproti datovému paralelismu. Vybrané instance nejspíše nejsou dostatečně obtížné, aby se vyplatilo řešení s vysokou režií jako má MPi paralelismus. 

# Závěr

Pokud vezmeme v úvahu obtížnost implementace a potřebné prostředky, jeví se jako nejvhodnější řešení pro daný problém datový paralelismus. Vykazuje značné zrychlení i bez příliš vysokého počtu výpočetních jader. Taskový paralelismus nelze doporučit, pro jeho vysokou míru náhody. MPI řešení vyžaduje veliké množství zdrojů a neposkytuje, alespoň na měřených instancích, dostatečné zrychlení.

# Přílohy

Veškeré zdrojové soubory spolu s použitými skripty jsou k nalezení na mém [gitlabu](https://gitlab.fit.cvut.cz/panekmi3/pdp)

