import matplotlib.pyplot as plt
from collections import Counter
import numpy as np

if __name__ == '__main__':

    data ={
        "1" : 10195,
        "2" : 6409,
        "4" : 2736,
        "8" : 1503,
        "16":  935,
        "24":  846,
        "32": 771,
        "48": 729
    }

    threads = list(data.keys())
    values = list(data.values())
    
    fig = plt.figure(figsize = (10, 5))

    plt.bar(threads, values, color ='maroon', 
        width = 0.4)
    plt.xlabel("Number of threads")
    plt.ylabel("Time in miliseconds")
    plt.title("Execution time of dat on in_0015.txt")

    plt.savefig("imgs/dat_0015.png")



    mpi_3 = {
        "3_6"     : 10841,
        "3_12"	: 10301,
        "3_24"	: 9529,
        "3_48"	: 9646,
        "4_6"  :	4605,
        "4_12" :	4346,
        "4_24" :	4010,
        "4_48" :	3752,
        "6_6"  :  3884,
        "6_12" :	3889,
        "6_24" :	3326,
        "6_48" :	4049
    }

    fig = plt.figure(figsize = (10, 5))

    plt.bar(mpi_3.keys(), mpi_3.values(), color ='maroon', width = 0.4, )
    
    plt.xlabel("Number of nodes and threads")
    plt.ylabel("Time in miliseconds")
    plt.title("Execution time of mpi on in_0015.txt")

    plt.savefig("imgs/mpi_0015.png")